#!/bin/bash
DXPCHAIND="/usr/local/bin/blockproducer_node"

# For blockchain download
VERSION=`cat /etc/dxpchain/version`

## Supported Environmental Variables
#
#   * $DXPCHAIND_SEED_NODES
#   * $DXPCHAIND_RPC_ENDPOINT
#   * $DXPCHAIND_PLUGINS
#   * $DXPCHAIND_REPLAY
#   * $DXPCHAIND_RESYNC
#   * $DXPCHAIND_P2P_ENDPOINT
#   * $DXPCHAIND_BLOCKPRODUCER_ID
#   * $DXPCHAIND_PRIVATE_KEY
#   * $DXPCHAIND_TRACK_ACCOUNTS
#   * $DXPCHAIND_PARTIAL_OPERATIONS
#   * $DXPCHAIND_MAX_OPS_PER_ACCOUNT
#   * $DXPCHAIND_ES_NODE_URL
#   * $DXPCHAIND_ES_START_AFTER_BLOCK
#   * $DXPCHAIND_TRUSTED_NODE
#

ARGS=""
# Translate environmental variables
if [[ ! -z "$DXPCHAIND_SEED_NODES" ]]; then
    for NODE in $DXPCHAIND_SEED_NODES ; do
        ARGS+=" --seed-node=$NODE"
    done
fi
if [[ ! -z "$DXPCHAIND_RPC_ENDPOINT" ]]; then
    ARGS+=" --rpc-endpoint=${DXPCHAIND_RPC_ENDPOINT}"
fi

if [[ ! -z "$DXPCHAIND_REPLAY" ]]; then
    ARGS+=" --replay-blockchain"
fi

if [[ ! -z "$DXPCHAIND_RESYNC" ]]; then
    ARGS+=" --resync-blockchain"
fi

if [[ ! -z "$DXPCHAIND_P2P_ENDPOINT" ]]; then
    ARGS+=" --p2p-endpoint=${DXPCHAIND_P2P_ENDPOINT}"
fi

if [[ ! -z "$DXPCHAIND_BLOCKPRODUCER_ID" ]]; then
    ARGS+=" --blockproducer-id=$DXPCHAIND_BLOCKPRODUCER_ID"
fi

if [[ ! -z "$DXPCHAIND_PRIVATE_KEY" ]]; then
    ARGS+=" --private-key=$DXPCHAIND_PRIVATE_KEY"
fi

if [[ ! -z "$DXPCHAIND_TRACK_ACCOUNTS" ]]; then
    for ACCOUNT in $DXPCHAIND_TRACK_ACCOUNTS ; do
        ARGS+=" --track-account=$ACCOUNT"
    done
fi

if [[ ! -z "$DXPCHAIND_PARTIAL_OPERATIONS" ]]; then
    ARGS+=" --partial-operations=${DXPCHAIND_PARTIAL_OPERATIONS}"
fi

if [[ ! -z "$DXPCHAIND_MAX_OPS_PER_ACCOUNT" ]]; then
    ARGS+=" --max-ops-per-account=${DXPCHAIND_MAX_OPS_PER_ACCOUNT}"
fi

if [[ ! -z "$DXPCHAIND_ES_NODE_URL" ]]; then
    ARGS+=" --elasticsearch-node-url=${DXPCHAIND_ES_NODE_URL}"
fi

if [[ ! -z "$DXPCHAIND_ES_START_AFTER_BLOCK" ]]; then
    ARGS+=" --elasticsearch-start-es-after-block=${DXPCHAIND_ES_START_AFTER_BLOCK}"
fi

if [[ ! -z "$DXPCHAIND_TRUSTED_NODE" ]]; then
    ARGS+=" --trusted-node=${DXPCHAIND_TRUSTED_NODE}"
fi

## Link the dxpchain config file into home
## This link has been created in Dockerfile, already
ln -f -s /etc/dxpchain/config.ini /var/lib/dxpchain
ln -f -s /etc/dxpchain/logging.ini /var/lib/dxpchain

# Plugins need to be provided in a space-separated list, which
# makes it necessary to write it like this
if [[ ! -z "$DXPCHAIND_PLUGINS" ]]; then
   exec "$DXPCHAIND" --data-dir "${HOME}" ${ARGS} ${DXPCHAIND_ARGS} --plugins "${DXPCHAIND_PLUGINS}"
else
   exec "$DXPCHAIND" --data-dir "${HOME}" ${ARGS} ${DXPCHAIND_ARGS}
fi

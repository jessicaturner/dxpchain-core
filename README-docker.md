# Docker Container

This repository comes with built-in Dockerfile to support docker
containers. This README serves as documentation.

## Dockerfile Specifications

The `Dockerfile` performs the following steps:

1. Obtain base image (phusion/baseimage:0.10.1)
2. Install required dependencies using `apt-get`
3. Add dxpchain-core source code into container
4. Update git submodules
5. Perform `cmake` with build type `Release`
6. Run `make` and `make_install` (this will install binaries into `/usr/local/bin`
7. Purge source code off the container
8. Add a local dxpchain user and set `$HOME` to `/var/lib/dxpchain`
9. Make `/var/lib/dxpchain` and `/etc/dxpchain` a docker *volume*
10. Expose ports `8090` and `1776`
11. Add default config from `docker/default_config.ini` and
    `docker/default_logging.ini`
12. Add an entry point script
13. Run the entry point script by default

The entry point simplifies the use of parameters for the `blockproducer_node`
(which is run by default when spinning up the container).

### Supported Environmental Variables

* `$DXPCHAIND_SEED_NODES`
* `$DXPCHAIND_RPC_ENDPOINT`
* `$DXPCHAIND_PLUGINS`
* `$DXPCHAIND_REPLAY`
* `$DXPCHAIND_RESYNC`
* `$DXPCHAIND_P2P_ENDPOINT`
* `$DXPCHAIND_BLOCKPRODUCER_ID`
* `$DXPCHAIND_PRIVATE_KEY`
* `$DXPCHAIND_TRACK_ACCOUNTS`
* `$DXPCHAIND_PARTIAL_OPERATIONS`
* `$DXPCHAIND_MAX_OPS_PER_ACCOUNT`
* `$DXPCHAIND_ES_NODE_URL`
* `$DXPCHAIND_TRUSTED_NODE`

### Default config

The default configuration is:

    p2p-endpoint = 0.0.0.0:1776
    rpc-endpoint = 0.0.0.0:8090
    bucket-size = [60,300,900,1800,3600,14400,86400]
    history-per-size = 1000
    max-ops-per-account = 100
    partial-operations = true

# Docker Compose

With docker compose, multiple nodes can be managed with a single
`docker-compose.yaml` file:

    version: '3'
    services:
     main:
      # Image to run
      image: dxpchain/dxpchain-core:latest
      # 
      volumes:
       - ./docker/conf/:/etc/dxpchain/
      # Optional parameters
      environment:
       - DXPCHAIND_ARGS=--help


    version: '3'
    services:
     fullnode:
      # Image to run
      image: dxpchain/dxpchain-core:latest
      environment:
      # Optional parameters
      environment:
       - DXPCHAIND_ARGS=--help
      ports:
       - "0.0.0.0:8090:8090"
      volumes:
      - "dxpchain-fullnode:/var/lib/dxpchain"


# Docker Hub

This container is properly registered with docker hub under the name:

* [dxpchain/dxpchain-core](https://hub.docker.com/r/dxpchain/dxpchain-core/)

Going forward, every release tag as well as all pushes to `develop` and
`testnet` will be built into ready-to-run containers, there.

# Docker Compose

One can use docker compose to setup a trusted full node together with a
delayed node like this:

```
version: '3'
services:

 fullnode:
  image: dxpchain/dxpchain-core:latest
  ports:
   - "0.0.0.0:8090:8090"
  volumes:
  - "dxpchain-fullnode:/var/lib/dxpchain"

 delayed_node:
  image: dxpchain/dxpchain-core:latest
  environment:
   - 'DXPCHAIND_PLUGINS=delayed_node blockproducer'
   - 'DXPCHAIND_TRUSTED_NODE=ws://fullnode:8090'
  ports:
   - "0.0.0.0:8091:8090"
  volumes:
  - "dxpchain-delayed_node:/var/lib/dxpchain"
  links: 
  - fullnode

volumes:
 dxpchain-fullnode:
```
